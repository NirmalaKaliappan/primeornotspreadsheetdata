import java.io.*;
import java.lang.*;
import java.util.*;


import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.apache.poi.ss.formula.functions.Column;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellAddress;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


class PrimeOrNot {
    //method to check the number is prime or not
    static String checkPrime(int n) {
        int i, m = 0, flag = 0;
        String primeOrNot = "";
        m = n / 2;
        if (n == 0 || n == 1) {
            System.out.println(n + " is not prime number");
        } else {
            for (i = 2; i <= m; i++) {
                if (n % i == 0) {
                    System.out.println(n + " is not prime number");
                    flag = 1;
                    primeOrNot = n + " is not a prime";
                    break;
                }
            }
            if (flag == 0) {
                System.out.println(n + " is prime number");
                primeOrNot = n + " is a prime";
            }
        }
        return primeOrNot;
    }

    public static void main(String args[]) throws IOException {
        Map<String, Object[]> datas = new TreeMap<>();
        int o = 0;
        Scanner input = new Scanner(System.in); //get the input from user
        System.out.println("Enter file path ends with filename:(ex: c:user\\desktop\\scp.xls):");
        String fileName = input.nextLine();

        FileInputStream fis = new FileInputStream(new File(fileName));
        //new File("/home/nirmala/Desktop/PrimeOrNot/vzorek_dat_úloha_SDC.xlsx"));
        XSSFWorkbook wb = new XSSFWorkbook(fis);
        XSSFSheet sheet = wb.getSheetAt(0);
        FormulaEvaluator formulaEvaluator = wb.getCreationHelper().createFormulaEvaluator();
        for (Row row : sheet) {
            for (Cell cell : row) {
                switch (formulaEvaluator.evaluateInCell(cell).getCellType()) {
                    case Cell.CELL_TYPE_NUMERIC:
                        System.out.print(cell.getNumericCellValue() + "\t\t");
                        break;
                    case Cell.CELL_TYPE_STRING:
                        System.out.print(cell.getStringCellValue() + "\t\t");
                        if (cell.getStringCellValue().matches("\\d+")) { //if the given cell content is digit then add it. if not, follow the else loop
                            String primeOrNotCheck = checkPrime(Integer.parseInt(cell.getStringCellValue()));
                            datas.put(String.valueOf(o), new Object[]{primeOrNotCheck});
                            o++;
                        } else {
                            String wordValues = cell.getStringCellValue();
                            datas.put(String.valueOf(o), new Object[]{wordValues});
                            o++;
                        }
                        break;
                }
                fis.close();
            }

        }
        writeContent(datas, fileName); //writing the content to xls
    }

//write method
    static void writeContent(Map<String, Object[]> maap, String novyFile) throws IOException {
        File file = new File(novyFile);
        String sep = System.getProperty("file.separator");
        String file_name = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf(sep) + 1);
        System.out.println(file_name);
        int count_ = file.getAbsolutePath().length() - (file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf(sep) + 1).length());
        String seperate_file_name = file_name.split("\\.")[0];
        file.delete();
        String dup_file_name = file.getAbsolutePath().substring(0, count_) + seperate_file_name + "." + (file_name.split("\\."))[1];

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet spreadsheet = workbook.createSheet("Data");
        CellStyle style = workbook.createCellStyle(); //if the cell content is word then it follows the style-bold
        Font font = workbook.createFont();
        font.setFontHeightInPoints((short) 11);
        font.setFontName(HSSFFont.FONT_ARIAL);
        font.setBold(true);
        font.setColor(HSSFColor.BLACK.index);
        style.setFont(font);

        XSSFRow row;
        Set<String> keyid = maap.keySet();
        int rowId = 0;

        for (String key : keyid) {
            Object[] objectArr = maap.get(key);
            int cellId = 1;
            row = spreadsheet.createRow(rowId++);
            for (Object obj : objectArr) {
                Cell cell = row.createCell(cellId);
                List<String> pp = new ArrayList<>();
                pp.add(new String(obj.toString()));
                System.out.println(pp);
                if (pp.get(0).matches("\\w+")) { //if the content match with word the it prints in bold style
                    cell.setCellStyle(style);
                }
                cell.setCellValue(obj.toString());
            }
        }
        try (FileOutputStream outputStream = new FileOutputStream(new File(dup_file_name))) {
            workbook.write(outputStream);
            outputStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
