# PrimeOrNotSpreadsheetData

Check the given number is prime or not- spreadsheet stored data

Steps to follow:

when you run the code,It asks the user to give the input file name (example: /home/nirmala/Desktop/PrimeOrNot/vzorek_dat_úloha_SDC.xlsx --for linux)
Enter the file name and press enter.
Apache libraries to create workbook-sheet-row-column is used
if the given cell content is digit, then it proceeds with checking the prime or not
If the given cell content is word, it just saves it in list for near future use
write the data into workbook.
write method operation:

It get's the filename at the beginning of the class and saves it for future use.
separating the file name with location and using it to save the new data(i.e, prime or not content)
row is created and column as well and follows by incrementing row and column. 4.Inbetween bold-style is applied to content( if the cell content starts with word then it follows the bold style)
Once the row-column details are stored in separate list( when the content is end in the loop),It tries to create the file with the same name(step 1)
Libraries needed:

commons-collections4-4.1.jar
poi-3.17.jar
poi-ooxml-3.17.jar
poi-ooxml-schemas-3.17.jar
xmlbeans-2.6.0.jar
